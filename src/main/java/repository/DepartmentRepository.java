package repository;

import static utils.DatabaseUtils.DATABASE_HOST;
import static utils.DatabaseUtils.DATABASE_PASSWORD;
import static utils.DatabaseUtils.DATABASE_USERNAME;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import domain.Department;

public class DepartmentRepository {

    private String selectAllFromDepartments = "Select * FROM departments";

    public List<Department> findAll() {
        List<Department> result = new ArrayList<>();
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(selectAllFromDepartments)) {

            while (rs.next()) {
                Integer deptId = rs.getInt("departmentId");
                String deptName = rs.getString("name");
                Department department = new Department(deptId, deptName);
                result.add(department);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return result;
    }


    public Department findById(Integer deptId){
        Department department= new Department();
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
             PreparedStatement stmt = conn.prepareStatement("select * from departments where departmentId = ?")
            ) {
            stmt.setInt(1, deptId);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Integer deptartmentId = rs.getInt("departmentId");
                String deptName = rs.getString("name");
                department = new Department(deptartmentId, deptName);
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return department;
    }
    public Department findByName(String name){

        Department department= new Department();
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
             PreparedStatement stmt = conn.prepareStatement("select * from departments where name = ?")
        ) {
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Integer deptartmentId = rs.getInt("departmentId");
                String deptName = rs.getString("name");
                department = new Department(deptartmentId, deptName);
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        return department;
    }

    public Department findByIdAndName(Integer deptId, String name){
        Department department= new Department();
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
             PreparedStatement stmt = conn.prepareStatement("select * from departments where departmentId = ? and name = ?")
        ) {
            stmt.setInt(1, deptId);
            stmt.setString(2, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                Integer deptartmentId = rs.getInt("departmentId");
                String deptName = rs.getString("name");
                department = new Department(deptartmentId, deptName);
            }
            rs.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return department;
    }


    public void updateDepartment(String nameToUpdate) {
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD);
             Statement stmt = conn.createStatement()) {
            String updateDepartment = "update departments set name=" + "'" + nameToUpdate + "'" + " where departmentId = 1";
            int affectedRows = stmt.executeUpdate(updateDepartment);

            System.out.println("rows affected: " + affectedRows);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void save(Department department){

        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD)) {
            String saveSql = "INSERT INTO departments(name) values(?)";
            PreparedStatement stmt = conn.prepareStatement(saveSql);
            stmt.setString(1, department.getName());
            int affectedRows = stmt.executeUpdate();
            System.out.println("rows affected: " + affectedRows);

        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void deleteById(Integer deptId){
        try (Connection conn = DriverManager.getConnection(DATABASE_HOST, DATABASE_USERNAME, DATABASE_PASSWORD)) {
            PreparedStatement stmt = conn.prepareStatement("DELETE from departments where departmentId = ?");
            stmt.setInt(1, deptId);
            int affectedRows = stmt.executeUpdate();
            System.out.println("rows affected: " + affectedRows);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

}
